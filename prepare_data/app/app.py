"""
This is a sample hello world app
It prints hello world 100 times
"""
__author__ = "Aashish Amber"

import time
import pandas as pd 
import numpy as np
import warnings
import os
from pathlib import Path
warnings.filterwarnings('ignore')
import argparse
import logging


#Creating and instance of that class
logger = logging.getLogger()

path = os.getcwd()
#path = Path(path).parent

def prepare_data(name):
  path_data = os.path.join(path,'data',name+'.data')
  item_data = os.path.join(path ,'data', name+'.item')

  df = pd.read_csv(path_data, sep='\t', names=['user_id','item_id','rating','titmestamp'])
  movie_cols = ['movie_id', 'movie_title', 'release_data', 'video release date', 'imdb_url', 'unknown', 'Action', 'Adventure', 'Animation', 'Children\'s', 'Comedy', 'Crime', 'Documentary', 'Drama', 'Fantasy', 'Film-Noir', 'Horror', 'Musical', 'Mystery', 'Romance', 'Sci-Fi', 'Thriller', 'War', 'Western']
  movie_titles = pd.read_csv(item_data, sep='|', names=movie_cols, encoding='latin-1')
  print(movie_titles)
  logger.info('Data Prepocessing half-way completed')
  
    


if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Get data versioning args')
  # Add the arguments
  parser.add_argument('commit')
  parser.add_argument('repo')
  parser.add_argument('branch')
  args = parser.parse_args()
  logger.info('args are',args)
  print('args are',args)
  print('Data Prepocessing started')
  prepare_data(args.commit)
  COUNT_TIMES = 100
  while COUNT_TIMES:
    COUNT_TIMES-=1
  time.sleep(120)
  print('Data Prepocessing completed')
