"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""
__author__ = "Aashish Amber"

import json
from json import JSONDecodeError
import logging
from flask import Flask
from flask import request
import os
import numpy as np
import pandas as pd 
from pathlib import Path
import re
import warnings
warnings.filterwarnings('ignore')
import boto3


path = os.getcwd()
config_file = os.path.join(path, 'config/dev.json')
cfg_fs = open(config_file, 'r')
config = json.load(cfg_fs)

print(config)

comprehend = boto3.client( aws_access_key_id=config['aws_access_key_id'], aws_secret_access_key=config['aws_secret_access_key'],service_name='comprehend', region_name='us-east-2')


#path = Path(path).parent
print(path)
path_data = os.path.join(path, 'data/u.data')
item_data = os.path.join(path, 'data/u.item')

df = pd.read_csv(path_data, sep='\t', names=['user_id','item_id','rating','titmestamp'])
movie_cols = ['movie_id', 'movie_title', 'release_data', 'video release date', 'imdb_url', 'unknown', 'Action', 'Adventure', 'Animation', 'Children\'s', 'Comedy', 'Crime', 'Documentary', 'Drama', 'Fantasy', 'Film-Noir', 'Horror', 'Musical', 'Mystery', 'Romance', 'Sci-Fi', 'Thriller', 'War', 'Western']
movie_titles = pd.read_csv(item_data, sep='|', names=movie_cols, encoding='latin-1')
df = pd.merge(df, movie_titles, how='left', left_on='item_id', right_on='movie_id', suffixes=('',''))
ratings = pd.DataFrame(df.groupby('movie_title')['rating'].mean())
ratings['Positive_reviews'] = round(df.groupby('movie_title')['rating'].count()/10).astype(int)
movie_matrix = df.pivot_table(index='user_id', columns='movie_title', values='rating')


def recommendation(title):
    result = pd.DataFrame()
    print(movie_matrix.columns)
    if title in movie_matrix.columns :
        user_rating = movie_matrix[title]
        similar = movie_matrix.corrwith(user_rating)
        corr_contact = pd.DataFrame(similar, columns=['Correlation'])
        corr_contact.dropna(inplace=True)
        corr_contact = corr_contact.join(ratings['Positive_reviews'])
        corr_contact.head()
        result = corr_contact[corr_contact['Positive_reviews'] > 5].sort_values(by='Correlation', ascending=False).head(10)
    print('result is ',result)    
    return result


  
def prepare_result(result):
    return result.to_json(orient='index')


def create_app() -> Flask:
    """
    Method to initialize the flask app. It should contain all the flask
    configuration

    Returns:
         Flask: instance of Flask application
    """
    flask_app = Flask(__name__)
    return flask_app


app = create_app()


@app.route('/movie_sentiment', methods=['Post'])
def sentiment():
    logger = logging.getLogger()
    content = request.get_json()
    logger.info("Received request from {}".format(content))
    dictionary = {}
    movie = content['review']
    movie = movie.strip()
    movie = re.sub(r'\\n', '\n', movie)
    print(movie)
    msg = 'ok'
    text = movie
    if text :
        print('Calling DetectSentiment')
        result = comprehend.detect_sentiment(Text=text, LanguageCode='en')
        print(json.dumps(result, sort_keys=True, indent=4))
        print('End of DetectSentiment\n')
        dictionary = {'sentiment': result['Sentiment'], 'status': msg, 'review' : text }
    else:
        msg = 'Movie not in database'
        dictionary = {'sentiment': '' , 'status':msg , 'review' : text}
    return json.dumps(dictionary)


@app.route('/recommend_movie', methods=['Post'])
def find_tickets():
    logger = logging.getLogger()
    content = request.get_json()
    logger.info("Received request from {}".format(content))
    dictionary = {}
    title = content['title']
    title = title.strip()
    title = re.sub(r'\\n', '\n', title)
    print(title)
    msg = 'ok'
    result = recommendation(title)
    if not result.empty :
        final_result = prepare_result(result) 
        dictionary = {'result': final_result, 'status': msg}
    else:
        msg = 'Movie not in database'
        dictionary = {'result': [] , 'status': msg}
    return json.dumps(dictionary)


if __name__ == '__main__':
    app.run(host="0.0.0.0",port=8760)
