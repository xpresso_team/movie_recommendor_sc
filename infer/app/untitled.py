"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""
__author__ = "Naveen Sinha"

import json
from json import JSONDecodeError
import logging
from flask import Flask
from flask import request

config_file = 'config/dev.json'

path = os.getcwd()

df = pd.read_csv(path_data, sep='\t', names=['user_id','item_id','rating','titmestamp'])
movie_cols = ['movie_id', 'movie_title', 'release_data', 'video release date', 'imdb_url', 'unknown', 'Action', 'Adventure', 'Animation', 'Children\'s', 'Comedy', 'Crime', 'Documentary', 'Drama', 'Fantasy', 'Film-Noir', 'Horror', 'Musical', 'Mystery', 'Romance', 'Sci-Fi', 'Thriller', 'War', 'Western']
movie_titles = pd.read_csv(item_data, sep='|', names=movie_cols, encoding='latin-1')
df = pd.merge(df, movie_titles, how='left', left_on='item_id', right_on='movie_id', suffixes=('',''))
ratings = pd.DataFrame(df.groupby('movie_title')['rating'].mean())
ratings['number_of_ratings'] = df.groupby('movie_title')['rating'].count()
movie_matrix = df.pivot_table(index='user_id', columns='movie_title', values='rating')


def recommendation(title):
    result = ''
    if movie_matrix[title] :
        user_rating = movie_matrix[title]
        similar = movie_matrix.corrwith(user_rating)
        corr_contact = pd.DataFrame(similar, columns=['Correlation'])
        corr_contact.dropna(inplace=True)
        corr_contact = corr_contact.join(ratings['number_of_ratings'])
        corr_contact.head()
        result = corr_contact[corr_contact['number_of_ratings'] > 100].sort_values(by='Correlation', ascending=False).head(10)
    return result


  
def prepare_result(result):
    return result.to_json(orient='index')


def create_app() -> Flask:
    """
    Method to initialize the flask app. It should contain all the flask
    configuration

    Returns:
         Flask: instance of Flask application
    """
    flask_app = Flask(__name__)
    return flask_app


app = create_app()


@app.route('/recommend_movie', methods=['Post'])
def find_tickets():
    logger = logging.getLogger()
    content = request.get_json()
    logger.info("Received request from {}".format(content))
    dictionary = {}
    title = content['title'].tolower()
    title = re.sub(r'\\n', '\n', title)
    print(title)
    msg = 'ok'
    result = recommendation(title)
    if result :
        final_result = prepare_result(result, 0) 
        dictionary = {'result': final_result, 'messages': msg}
    else:
        msg = 'no valid title &  desc'
        dictionary = {'result': [] , 'message':'Movie not in database'}
    return json.dumps(dictionary)


if __name__ == '__main__':
    app.run(host="0.0.0.0",port=8760)
